export interface toDo{
    name: string,
    description: string,
    dueDate: string,
    priority: string,
    status: boolean
}