import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { ToDoEditComponent } from './to-do-list/to-do-edit/to-do-edit.component';
import { BulkActionComponent } from './to-do-list/bulk-action/bulk-action.component';

@NgModule({
  declarations: [
    AppComponent,
    NewTaskComponent,
    ToDoListComponent,
    ToDoEditComponent,
    BulkActionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
