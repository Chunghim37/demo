import { Injectable } from '@angular/core';
import { toDo } from '../toDoApp.model';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {

  constructor() { }

  private toDos: toDo[] = [
    {
      name: "Do Homework",
      description: "Do all homework...",
      dueDate: "29/3/2021",
      priority: "normal",
      status: false,
    },
    {
      name: "Do Housework",
      description: "Do all housework...",
      dueDate: "31/3/2021",
      priority: "normal",
      status: false,
    },
    {
      name: "Learn Something",
      description: "Learn English, Math, Geography,...",
      dueDate: "30/3/2021",
      priority: "normal",
      status: false,
    } 
  ];

  getToDo(): toDo[] {
    return this.toDos;
  }

  newToDo(value: toDo) {
    this.toDos.push(value);
  }
}
