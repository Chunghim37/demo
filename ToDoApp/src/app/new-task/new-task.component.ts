import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToDoService } from '../services/to-do.service';
import { toDo } from '../toDoApp.model';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css'],
})
export class NewTaskComponent implements OnInit {
  toDoForm!: FormGroup;
  toDo: toDo[] = [];

  today: number = Date.now();
  priorities: string[] = ['Normal', 'Low', 'High'];

  constructor(
    private formBuilder: FormBuilder,
    private toDoItem: ToDoService
  ) {}

  ngOnInit(): void {
    this.toDoForm = this.formBuilder.group({
      name: this.formBuilder.control('', Validators.required),
      description: this.formBuilder.control('', Validators.required),
      dueDate: this.formBuilder.control(''),
      priority: this.formBuilder.control(''),
      status: false,
    });
  }

  onSubmit(value: any): any {
    this.toDoItem.newToDo(this.toDoForm.value);
    this.toDoForm.reset();
  }
}
