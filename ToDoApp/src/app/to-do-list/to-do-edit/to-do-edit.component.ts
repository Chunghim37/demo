import { Component, OnInit } from '@angular/core';
import { ToDoService } from 'src/app/services/to-do.service';
import { toDo } from 'src/app/toDoApp.model';

@Component({
  selector: 'app-to-do-edit',
  templateUrl: './to-do-edit.component.html',
  styleUrls: ['./to-do-edit.component.css']
})
export class ToDoEditComponent implements OnInit {

  toDos: toDo[] = [];

  constructor(private toDo: ToDoService) {
    this.toDos = toDo.getToDo();
  }

  ngOnInit(): void {}

  handlerChange(item: toDo) {
    item.status = !item.status;
  }

  removeToDo(index: number) {
    this.toDos.splice(index, 1);
  }

}
