import { Component, OnInit } from '@angular/core';
import { ToDoService } from '../services/to-do.service';
import { toDo } from '../toDoApp.model';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css'],
})
export class ToDoListComponent implements OnInit {
  toDos: toDo[] = [];
  toDoItem!: number;

  constructor(private toDo: ToDoService) {
    this.toDos = toDo.getToDo().sort(this.softDate);
  }

  ngOnInit(): void {}

  softDate(a: toDo, b: toDo) {
    if (a.dueDate.toUpperCase() > b.dueDate.toUpperCase()) {
      return 1;
    } else {
      return -1;
    }
  }
  
  handlerChange(item: toDo) {
    item.status = !item.status;
  }

  detailToDo(index: number) {}

  removeToDo(index: number) {
    this.toDos.splice(index, 1);
  }
}
